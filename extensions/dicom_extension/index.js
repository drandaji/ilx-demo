/*
 * Template for iRulesLX RPC.
 *
 * Note: This example works in concert with the template provided in a
 * default ilx TCL iRule.
 */

/* Import the f5-nodejs module. */
var f5 = require('f5-nodejs');

/* Create a new rpc server for listening to TCL iRule calls. */
var ilx = new f5.ILXServer();

/*
 * Create and register a remote function to be triggered by a TCL
 * iRule ILX::call or ILX::notify.
 *
 * To use, uncomment, replace <REMOTE_FUNC_NAME>, and make sure
 * whatever it is replaced with appears in the corresponding iRule
 * ILX::call or ILX::notify calls.
 */


ilx.addMethod('dicom_decode', function(req, res) {
    // Function parameters can be found in req.params().
   var range = req.params();
   console.log("params: " + range); 
   // Whatever is placed in res.reply() will be the return value from ILX::call.
   res.reply('<RESPONSE>');
});


/* Start listening for ILX::call and ILX::notify events. */
ilx.listen();








