# v11 modified via gitlab
#
# A "Hello World" template for iRulesLX RPC.
#
# Note: This example works in concert with the template in an
# extension's default index.js.
#
# To use, replace every item in <> with an appropriate value.
#
when CLIENT_ACCEPTED {
    # Get a handle to the running extension instance to call into.
    set RPC_HANDLE [ILX::init "helloPlugin" ]
    # Make the call and store the response in $rpc_response
    set rpc_response [ILX::call $RPC_HANDLE "say_something" "hello world" ]
}

when RULE_INIT {
    puts "hello cloud"
}    